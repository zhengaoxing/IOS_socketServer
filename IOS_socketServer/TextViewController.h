//
//  TextViewController.h
//  IOS_socketServer
//
//  Created by Maculish Ting on 15/4/29.
//  Copyright (c) 2015年 LYD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UITextField *portField;
- (IBAction)startListen:(id)sender;

@end
